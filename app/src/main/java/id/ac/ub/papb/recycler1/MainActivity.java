package id.ac.ub.papb.recycler1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView rv1;
    public static String TAG = "RV1";
    private Button btn1;
    private EditText etNim;
    private EditText etNama;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindView();
        rv1 = findViewById(R.id.rv1);
        ArrayList<Mahasiswa> data = getData();
        refreshRecylerView(data);
        EditText nimEdit = this.etNim;
        EditText namaEdit = this.etNama;
        this.btn1.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {
                String textNim = nimEdit.getText().toString();
                String textNama = namaEdit.getText().toString();
                Mahasiswa mhs = new Mahasiswa();
                mhs.nim = textNim;
                mhs.nama = textNama;
                ArrayList<Mahasiswa> newData = data;
                newData.add(mhs);
                refreshRecylerView(newData);
            }
        });
    }
    private void bindView(){
        this.btn1 = findViewById(R.id.bt1);
        this.etNim = findViewById(R.id.etNim);
        this.etNama = findViewById(R.id.etNama);
    }
    private void refreshRecylerView(ArrayList<Mahasiswa> data){
        MahasiswaAdapter adapter = new MahasiswaAdapter(this, data);
        rv1.setAdapter(adapter);
        rv1.setLayoutManager(new LinearLayoutManager(this));
    }
    public ArrayList getData() {
        ArrayList<Mahasiswa> data = new ArrayList<>();
        List<String> nim = Arrays.asList(getResources().getStringArray(R.array.nim));
        List<String> nama = Arrays.asList(getResources().getStringArray(R.array.nama));
        for (int i = 0; i < nim.size(); i++) {
            Mahasiswa mhs = new Mahasiswa();
            mhs.nim = nim.get(i);
            mhs.nama = nama.get(i);
            Log.d(TAG,"getData "+mhs.nim);
            data.add(mhs);
        }
        return data;
    }
}